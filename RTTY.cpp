/*
 * RTTY.cpp
 *
 *  Created on: Apr 7, 2017
 *      Author: roland
 */

#include "RTTY.h"

#include <Arduino.h>

RTTY::RTTY(uint8_t pin)
	: _pin(pin)
{
	pinMode(_pin, OUTPUT);
}

void RTTY::send(char c)
{
	uint8_t bits = charToBits(c);

	sendSpace();

	for(int i = 0; i < 5; ++i)
	{
		if(bits & (1 << i))
			sendMark();
		else
			sendSpace();
	}

	sendMark();
}
void RTTY::send(const char* str)
{
	sendDiddle();
	while(*str != '\0')
	{
		send(*str);
		++str;
	}
	sendDiddle();
}

void RTTY::sendMark(void)
{
	tone(_pin, _markFreq);
	delay(_duration);
	noTone(_pin);
}

void RTTY::sendSpace(void)
{
	tone(_pin, _spaceFreq);
	delay(_duration);
	noTone(_pin);
}

void RTTY::sendDiddle(void)
{
	sendSpace();
	sendMark();
	sendMark();
	sendMark();
	sendMark();
	sendMark();
	sendMark();
}

uint8_t RTTY::charToBits(char c)
{
	char normalized = toLowerCase(c);

	//leters
	if('a' <= normalized && 'z' >= normalized)
		return _charLookup[normalized - 'a'];

	//digits
	if('0' <= normalized && '9' >= normalized)
		return _digitLookup[normalized - '0'];

	switch(normalized)
	{
	case '-':
		return 0b00011;
	case ' ':
		return 0b00100;
	case '?':
		return 0b11001;
	case ':':
		return 0b01110;
	case '(':
		return 0b01111;
	case ')':
		return 0b10010;
	case '.':
		return 0b11100;
	case ',':
		return 0b01100;
	case '/':
		return 0b11101;
	default:
		return 0b00100;	//space
	}
}
