/*
 * RTTY.h
 *
 *  Created on: Apr 7, 2017
 *      Author: roland
 */

#ifndef RTTY_H_
#define RTTY_H_

#include <Arduino.h>

class RTTY
{
public:
	RTTY(uint8_t pin);
	void send(char c);
	void send(const char* str);

//private:
	void sendMark(void);
	void sendSpace(void);
	void sendDiddle(void);

	uint8_t charToBits(char c);

	const unsigned int _markFreq = 1445;
	const unsigned int _spaceFreq = 1275;
	const uint16_t _duration = 22;		//in ms

	const uint8_t _charLookup[30] = {
			0b00011,	//A
			0b11001,	//B
			0b01110,	//C
			0b01001,	//D
			0b00001,	//E
			0b01101,	//F
			0b11010,	//G
			0b10100,	//H
			0b00110,	//I
			0b01011,	//J
			0b01111,	//K
			0b10010,	//L
			0b11100,	//M
			0b01100,	//N
			0b11000,	//O
			0b10110,	//P
			0b10111,	//Q
			0b01010,	//R
			0b00101,	//S
			0b10000,	//T
			0b00111,	//U
			0b11110,	//V
			0b10011,	//W
			0b11101,	//X
			0b10101,	//Y
			0b10001		//Z
	};

	const uint8_t _digitLookup[10] = {
			0b10110,	//0
			0b10111,	//1
			0b10011,	//2
			0b00001,	//3
			0b01010,	//4
			0b10000,	//5
			0b10101,	//6
			0b00111,	//7
			0b00110,	//8
			0b11000		//9
	};

	uint8_t _pin;
};



#endif /* RTTY_H_ */
